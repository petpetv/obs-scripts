--[==================================================[

	OBS version: 27.0.1

]==================================================]

if not table.unpack then
	table.unpack = unpack
end

local obs = obslua

local mediaSource = nil
local outputIndex = 63

local default = {
	s = 0, m = 0, h = 0, d= 0,
	text_source             = "",
	end_text                = "Finish!",
	group_format_visibility = false,
	sync_input              = true,
	format_type             = "3",
	padding_s               = 2,
	padding_m               = 2,
	padding_h               = 2,
	padding_d               = 2,
	separator_m             = ":",
	separator_h             = ":",
	separator_d             = ":",
	before_text             = "",
	after_text              = "",
	group_audio_visibility  = false,
	audio_enabled           = true,
	audio_source            = "",
	audio_volume            = 0.5,
	audio_looping           = false,
}

local type_def = {
	{"int", "s"},
	{"int", "m"},
	{"int", "h"},
	{"int", "d"},
	{"string", "text_source"},
	{"string", "end_text"},
	{"bool", "group_format_visibility"},
	{"bool", "sync_input"},
	{"string", "format_type"},
	{"int", "padding_s"},
	{"int", "padding_m"},
	{"int", "padding_h"},
	{"int", "padding_d"},
	{"string", "separator_m"},
	{"string", "separator_h"},
	{"string", "separator_d"},
	{"string", "before_text"},
	{"string", "after_text"},
	{"bool", "group_audio_visibility"},
	{"bool", "audio_enabled"},
	{"string", "audio_source"},
	{"double", "audio_volume"},
	{"bool", "audio_looping"},
}

local input = {}
setmetatable(input, {__index = default})

local cd_timer = {}
cd_timer.new = function ()
	local timer = {
		ts = 0, tm = 0, th = 0,
		is_running = false,
		format_list = {},
	}
	setmetatable(timer, {__index = input})
	local function f(t)
		return "%0" .. t .. "d"
	end
	local init = function (self)
		self.format_list["1"] = function ()
			local format = input.before_text
			..f(input.padding_d) .. input.separator_d
			..f(input.padding_h) .. input.separator_h
			..f(input.padding_m) .. input.separator_m
			..f(input.padding_s) .. input.after_text
			local success, result = pcall(string.format, format, self.d, self.h, self.m, self.s)
			if success then return result end
			return string.format("%02d:%02d:%02d:%02d", self.d, self.h, self.m, self.s)
		end
		self.format_list["2"] = function ()
			local format = input.before_text
			..f(input.padding_h) .. input.separator_h
			..f(input.padding_m) .. input.separator_m
			..f(input.padding_s) .. input.after_text
			local success, result = pcall(string.format, format, self.th, self.m, self.s)
			if success then return result end
			return string.format("%02d:%02d:%02d", self.th, self.m, self.s)
		end
		self.format_list["3"] = function ()
			local format = input.before_text
			..f(input.padding_m) .. input.separator_m
			..f(input.padding_s) .. input.after_text
			local success, result = pcall(string.format, format, self.tm, self.s)
			if success then return result end
			return string.format("%02d:%02d", self.tm, self.s)
		end
		self.format_list["4"] = function ()
			local format = input.before_text
			..f(input.padding_s) .. input.after_text
			local success, result = pcall(string.format, format, self.ts)
			if success then return result end
			return string.format("%02d", self.ts)
		end
		self.decrement = function ()
			self.ts = self.calc_duration() - 1
			self.tm = math.floor(self.ts / 60)
			self.th = math.floor(self.tm / 60)
			self.s = math.floor(self.ts % 60)
			self.m = math.floor(self.tm % 60)
			self.h = math.floor(self.th % 24)
			self.d = math.floor(self.th / 24)
		end
		self.set_time = function ()
			for _, v in pairs({"s", "m", "h", "d"}) do
				self[v] = input[v]
			end
			self.ts = self.calc_duration()
			self.tm = math.floor(self.ts / 60)
			self.th = math.floor(self.tm / 60)
		end
		self.calc_duration = function ()
			return self.s + (self.m * 60) + (self.h * 60 * 60) + (self.d * 24 * 60 * 60)
		end
		self.output_text = function ()
			local source = obs.obs_get_source_by_name(input.text_source)
			if source ~= nil then
				local text = self.format_list[input.format_type]()
				local settings = obs.obs_data_create()
				if self.ts <= 0 and input.end_text ~= "" then
					text = input.end_text
				end
				obs.obs_data_set_string(settings, "text", text)
				obs.obs_source_update(source, settings)
				obs.obs_data_release(settings)
				obs.obs_source_release(source)
			end
		end
		self.show_text = function ()
			self.set_time()
			self.output_text()
		end
		self.callback = function ()
			local source = obs.obs_get_source_by_name(input.text_source)
			if source ~= nil then
				self.decrement()
				self.output_text()
				if self.ts <= 0 then
					self.is_running = false
					obs.remove_current_callback()
					if input.audio_enabled and input.audio_source ~= "" then
						play_sound()
					end
				end
				obs.obs_source_release(source)
			end
		end
		self.start_stop = function ()
			if self.is_running then
				obs.timer_remove(self.callback)
			else
				self.output_text()
				obs.timer_add(self.callback, 1000)
			end
			self.is_running = not self.is_running
		end
		self.reset = function ()
			stop_sound()
			if self.is_running then
				self.is_running = false
				obs.timer_remove(self.callback)
			end
			local source = obs.obs_get_source_by_name(input.text_source)
			if source ~= nil then
				self.show_text()
				obs.obs_source_release(source)
			end
		end
		return self
	end
	return init(timer)
end

local cdTimer = cd_timer.new()

function update_sound ()
	local settings = obs.obs_data_create()
	obs.obs_data_set_string(settings, "local_file", input.audio_source)
	obs.obs_data_set_bool(settings, "looping", input.audio_looping)
	obs.obs_source_update(mediaSource, settings)
	obs.obs_data_release(settings)
end

function play_sound ()
	update_sound()
	obs.obs_source_remove(mediaSource) -- audio only
	obs.obs_source_set_enabled(mediaSource, true)
	obs.obs_set_output_source(outputIndex, mediaSource)
end

function stop_sound ()
	obs.obs_source_set_enabled(mediaSource, false)
end



------------------------------------------------------------

function script_properties ()
	local refresh_source_list = function (props)
		local prop = obs.obs_properties_get(props, "text_source")
		local sources = obs.obs_enum_sources()
		obs.obs_property_list_clear(prop)
		obs.obs_property_list_add_string(prop, "", "")
		if sources ~= nil then
			for _, source in ipairs(sources) do
				local source_id = obs.obs_source_get_id(source)
				if	source_id == "text_gdiplus" or
					source_id == "text_gdiplus_v2" or
					source_id == "text_ft2_source" then
						local name = obs.obs_source_get_name(source)
						obs.obs_property_list_add_string(prop, name, name)
				end
			end
		end
		obs.source_list_release(sources)
		return true
	end

	local set_blank_text = (function ()
		local old_text_source = input.text_source
		return function ()
			if old_text_source ~= input.text_source then
				local source = obs.obs_get_source_by_name(old_text_source)
				old_text_source = input.text_source
				if source ~= nil then
					local settings = obs.obs_data_create()
					obs.obs_data_set_string(settings, "text", "")
					obs.obs_source_update(source, settings)
					cdTimer.show_text()
					obs.obs_data_release(settings)
					obs.obs_source_release(source)
				end
			end
		end
	end)()

	local prop_sets = function (func)
		return function (name_table, val)
			return function (props, prop, settings)
				for _, name in ipairs(name_table) do
					func(obs.obs_properties_get(props, name), (function ()
						if type(val) == "function" then
							return val()
						else
							return val
						end
					end)())
				end
				return true
			end
		end
	end

	local sets_enabled = prop_sets(obs.obs_property_set_enabled)
	local sets_visibility = prop_sets(obs.obs_property_set_visible)

	local set_ga_enabled = sets_enabled(
		{"audio_source", "audio_volume", "audio_looping", "play_sound_button", "stop_sound_button"},
		function () return input.audio_enabled end
	)
	local set_ga_visibility = sets_visibility(
		{"audio_enabled", "audio_source", "audio_volume", "audio_looping", "play_sound_button", "stop_sound_button"},
		function () return input.group_audio_visibility end
	)
	local set_gf_type_visibility = function (props, prop, settings)
		sets_visibility({"format_s", "format_m", "format_h", "h", "format_d", "d"}, true)(props)
		local format_type = tonumber(input.format_type)
		local d, h, m = table.unpack({{"format_d"}, {"format_h"}, {"format_m"}})
		if input.sync_input then
			table.insert(d, "d")
			table.insert(h, "h")
		end
		if 1 < format_type then sets_visibility(d, false)(props) end
		if 2 < format_type then sets_visibility(h, false)(props) end
		if 3 < format_type then sets_visibility(m, false)(props) end
		return true
	end
	local set_gf_visibility = function (props, prop, settings)
		if input.group_format_visibility then
			sets_visibility({"sync_input", "format_type", "before_text", "after_text"}, true)(props)
			set_gf_type_visibility(props)
		else
			sets_visibility({"sync_input", "format_type", "format_s", "format_m",
			"format_h", "format_d", "before_text", "after_text"}, false)(props)
		end
		return true
	end

	local set_volume = function ()
		obs.obs_source_set_volume(mediaSource, input.audio_volume)
	end

	-- properties
	local root, g_format, g_audio, gf_d, gf_h, gf_m, gf_s = (function ()
		local tbl = {}
		repeat
			tbl[#tbl + 1] = obs.obs_properties_create()
		until #tbl == 7
		return table.unpack(tbl)
	end)()

	-- root
	local text_source, refresh_text_source, end_text, d, h, m, s, reset_button,
	start_stop_button, group_format_visibility, group_audio_visibility = table.unpack({
		obs.obs_properties_add_list(root, "text_source", "Source Name", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING),
		obs.obs_properties_add_button(root, "refresh_text_source", "Refresh list of sources", refresh_source_list),
		obs.obs_properties_add_text(root, "end_text", "End Text", obs.OBS_TEXT_DEFAULT),
		obs.obs_properties_add_int(root, "d", "Days", 0, 365, 1),
		obs.obs_properties_add_int(root, "h", "Hours", 0, 24-1, 1),
		obs.obs_properties_add_int(root, "m", "Minutes", 0, 60-1, 1),
		obs.obs_properties_add_int(root, "s", "Seconds", 0, 60-1, 1),
		obs.obs_properties_add_button(root, "reset_button", "Reset", cdTimer.reset),
		obs.obs_properties_add_button(root, "start_stop_button", "Start/Stop", cdTimer.start_stop),
		obs.obs_properties_add_group(root, "group_format_visibility", "Format Settings", obs.OBS_GROUP_CHECKABLE, g_format),
		obs.obs_properties_add_group(root, "group_audio_visibility", "Audio Settings", obs.OBS_GROUP_CHECKABLE, g_audio),
	})
	--- init
	refresh_source_list(root)
	--- set callback
	obs.obs_property_set_modified_callback(text_source, set_blank_text)
	obs.obs_property_set_modified_callback(group_format_visibility, set_gf_visibility)
	obs.obs_property_set_modified_callback(group_audio_visibility, set_ga_visibility)

	-- format
	local sync_input, format_type, before_text, format_h, format_m, format_s, after_text,
	padding_d, padding_h, padding_m, padding_s, separator_d, separator_h, separator_m = table.unpack({
		obs.obs_properties_add_bool(g_format, "sync_input", "Sync input fields.(Hours, Days)"),
		obs.obs_properties_add_list(g_format, "format_type", "Display Type", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING),
		obs.obs_properties_add_text(g_format, "before_text", "Before Text", obs.OBS_TEXT_DEFAULT),
		obs.obs_properties_add_group(g_format, "format_d", "Days", obs.OBS_GROUP_NORMAL, gf_d),
		obs.obs_properties_add_group(g_format, "format_h", "Hours", obs.OBS_GROUP_NORMAL, gf_h),
		obs.obs_properties_add_group(g_format, "format_m", "Minutes", obs.OBS_GROUP_NORMAL, gf_m),
		obs.obs_properties_add_group(g_format, "format_s", "Seconds", obs.OBS_GROUP_NORMAL, gf_s),
		obs.obs_properties_add_text(g_format, "after_text", "After Text", obs.OBS_TEXT_DEFAULT),
		obs.obs_properties_add_int(gf_d, "padding_d", "0 Padding", 1, 3+1, 1),
		obs.obs_properties_add_int(gf_h, "padding_h", "0 Padding", 1, 4+1, 1),
		obs.obs_properties_add_int(gf_m, "padding_m", "0 Padding", 1, 6+1, 1),
		obs.obs_properties_add_int(gf_s, "padding_s", "0 Padding", 1, 8+1, 1),
		obs.obs_properties_add_text(gf_d, "separator_d", "Separator", obs.OBS_TEXT_DEFAULT),
		obs.obs_properties_add_text(gf_h, "separator_h", "Separator", obs.OBS_TEXT_DEFAULT),
		obs.obs_properties_add_text(gf_m, "separator_m", "Separator", obs.OBS_TEXT_DEFAULT),
	})
	-- init
	for _, v in ipairs({sync_input, format_type, before_text, format_h, format_m, format_s, after_text}) do
		obs.obs_property_set_visible(v, input.group_format_visibility)
	end
	for i, v in ipairs({"+Days", "+Hours", "+Minutes", "Seconds Only",}) do
		obs.obs_property_list_add_string(format_type, v, i)
	end
	set_gf_type_visibility(root)
	set_gf_visibility(root)
	-- set callback
	obs.obs_property_set_modified_callback(sync_input, set_gf_type_visibility)
	obs.obs_property_set_modified_callback(format_type, set_gf_type_visibility)

	-- audio
	local audio_enabled, audio_source, audio_volume, audio_looping, play_sound_button, stop_sound_button = table.unpack({
		obs.obs_properties_add_bool(g_audio, "audio_enabled", "Enable Audio"),
		obs.obs_properties_add_path(g_audio, "audio_source", "Audio file", obs.OBS_PATH_FILE, "*", script_path()),
		obs.obs_properties_add_float_slider(g_audio, "audio_volume", "Volume", 0, 1, 0.01),
		obs.obs_properties_add_bool(g_audio, "audio_looping", "Looping"),
		obs.obs_properties_add_button(g_audio, "play_sound_button", "Test Play", play_sound),
		obs.obs_properties_add_button(g_audio, "stop_sound_button", "Stop Sound", stop_sound),
	})
	-- init
	for _,v in ipairs({audio_source, audio_volume, audio_looping, play_sound_button, stop_sound_button}) do
		obs.obs_property_set_enabled(v, input.audio_enabled)
	end
	for _,v in ipairs({audio_enabled, audio_source, audio_volume, audio_looping, play_sound_button, stop_sound_button}) do
		obs.obs_property_set_visible(v, input.group_audio_visibility)
	end
	-- set callback
	obs.obs_property_set_modified_callback(audio_enabled, set_ga_enabled)
	obs.obs_property_set_modified_callback(audio_volume, set_volume)

	return root
end

------------------------------------------------------------

-- for the "for loop"
-- use the same table key and property name
local obs_data = {
	set_default = {
		string = obs.obs_data_set_default_string,
		int    = obs.obs_data_set_default_int,
		double = obs.obs_data_set_default_double,
		bool   = obs.obs_data_set_default_bool,
		obj    = obs.obs_data_set_default_obj,
	},
	get = {
		string = obs.obs_data_get_string,
		int    = obs.obs_data_get_int,
		double = obs.obs_data_get_double,
		bool   = obs.obs_data_get_bool,
		obj    = obs.obs_data_get_obj,
		array  = obs.obs_data_get_array,
	}
}

------------------------------------------------------------

function script_defaults (settings)
	for _, v in ipairs(type_def) do
		local key, name = table.unpack(v)
		obs_data.set_default[key](settings, name, input[name])
	end
end

function script_update (settings)
	for _, v in ipairs(type_def) do
		local key, name = table.unpack(v)
		input[name] = obs_data.get[key](settings, name)
	end

	if not cdTimer.is_running then
		cdTimer.show_text()
	end
end

-- function script_save (settings) end

function script_load (settings)
	mediaSource = obs.obs_source_create_private("ffmpeg_source", "Global Media Source", nil)
	obs.obs_source_set_monitoring_type(mediaSource, obs.OBS_MONITORING_TYPE_MONITOR_AND_OUTPUT)
end

function script_unload (settings)
	obs.obs_set_output_source(outputIndex, nil)
	obs.obs_source_release(mediaSource)
end

function script_description ()
	return [[<div><h3>Countdown Timer</h3><div>Made by @petpetv</div>]]
end
