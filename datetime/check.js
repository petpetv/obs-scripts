/**
 * check the month, weekday and ampm notation.
 * $ node check.js
 * OR
 * $ deno run check.js
 * OR
 * Run it in your web browser's devTools.
 */
void function (month, weekday, ampm) {
    const ary = ["en-US", "es-ES", "ja-JP", "ko-KR", "ru-RU", "zh-CN", "zh-TW"]

    console.groupCollapsed(month)
    ary.map(code => {
        console.group(code)
        console.table([Array(12).fill().map((_, i) => i + 1)]
            .flatMap(v => [v, v])
            .map(v => v.map(x => new Date(Date.UTC(2021, x - 1))))
            .map((v, i) =>
                i == 0
                    ? v.map(x => new Intl.DateTimeFormat(code, { month: "long" }).format(x))
                    : v.map(x => new Intl.DateTimeFormat(code, { month: "short" }).format(x))
            ))
        console.groupEnd(code)
    })
    console.groupEnd(month)

    console.groupCollapsed(weekday)
    ary.map(code => {
        console.group(code)
        console.table([Array(7).fill().map((_, i) => i + 1)]
            .flatMap(v => [v, v])
            .map(v => v.map(x => new Date(Date.UTC(2021, 7, x))))
            .map((v, i) =>
                i == 0
                    ? v.map(x => new Intl.DateTimeFormat(code, { weekday: "long" }).format(x))
                    : v.map(x => new Intl.DateTimeFormat(code, { weekday: "short" }).format(x))
            ))
        console.groupEnd(code)
    })
    console.groupEnd(weekday)

    console.groupCollapsed(ampm)
    ary.map(code => {
        console.group(code)
        console.table([0, 12]
            .map(v => new Date(Date.UTC(2021, 7, 1, v)))
            .map(v => new Intl.DateTimeFormat(code, { hour: "numeric", hour12: true, timeZone: "Etc/UTC" }).format(v)))
        console.groupEnd(code)
    })
    console.groupEnd(ampm)
}("month", "weekday", "AMPM")
