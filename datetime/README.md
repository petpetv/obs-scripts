# OBS Studio scripts

OBS Studio is a free software designed for capturing, compositing, encoding,
recording, and streaming video content. More on https://obsproject.com.

This repository contains scripts to add functionnalities to OBS Studio.

## Howto install scripts in OBS Studio

- Download the script anywhere on your hard drive
- Go to Tools -> Scripts and add the downloaded script

## OBS Studio API for developers

- https://obsproject.com/wiki/Getting-Started-with-OBS-Studio-Development
- https://obsproject.com/docs/scripting.html

# 

- Derived from: https://gitlab.com/albinou/obs-scripts/
