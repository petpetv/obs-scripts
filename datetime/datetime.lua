--[[ OBS Studio datetime script

This script transforms a text source into a digital clock. The datetime format
is configurable and uses the same syntax than the Lua os.date() call.
]]

local obs               = obslua
local source_name       = ""
local default_format    = "%Y/%m/%d %H:%M:%S"
local datetime_format   = ""
local activated         = false

local note_visible      = false
local option_visible    = false
local month             = "None"
local wday              = "None"
local ampm              = "None"
local default_note      = [[The datetime format can use the following tags:
default = ]] .. default_format .. [[


%Y	full year
%y	2-digit year [00-99]
%m	month [01-12]
%b	month name (short)
%B	month name (long)
%d	day of the month [01-31]
%e	day of the month [1-31]
%a	weekday name (short)
%A	weekday name (long)
%k	hour, 24-cycle [01-24]
%H	hour, 24-cycle [00-23]
%I	hour, 12-cycle [01-12]
%l	hour, 12-cycle [00-11]
%M	minute [00-59]
%S	second [00-61]

%j	day of the year [1-365]
%u	weekday number [1-7 = Mon-Sun]
%w	weekday number [0-6 = Sun-Sat]
%p	AM/PM
%P	am/pm
%z	UTC offset

%x	date = %m/%d/%y
%r	time, 12-cycle = %I:%M:%S %p
%R	time = %H:%M
%T	time = %H:%M:%S
%X	time = %H:%M:%S
%c	date & time = %a %b %e %H:%M:%S %Y

%n	new line
%%	`%´ character]]

if not table.unpack then
    table.unpack = unpack
end

-- とりあえず 話者数上位3カ国+α
-- javascript の DateTimeFormat を参考
local locale, locale_keys = (function ()
    local _wday = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}
    local _month = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
    local es_ES = (function (t)
        local wday = {"domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"}
        local month_l = {"enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"}
        local month_s = {"ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sept", "oct", "nov", "dic"}
        for i, v in ipairs(_wday) do
            t[v], t[v:sub(1,3)] = wday[i], wday[i]:sub(1,3)
        end
        for i, v in ipairs(_month) do
            t[v], t[v:sub(1,3)] = month_l[i], month_s[i]
        end
        t.AM, t.PM = "a. m.", "p. m."
        return t
    end)({})
    local ja_JP = (function (t)
        local wday = {"日", "月", "火", "水", "木", "金", "土"}
        for i, v in ipairs(_wday) do
            t[v] ,t[v:sub(1,3)] = wday[i] .. "曜日", wday[i]
        end
        for i, v in ipairs(_month) do
            t[v], t[v:sub(1,3)] = i .. "月", i .. "月"
        end
        t.AM, t.PM = "午前", "午後"
        return t
    end)({})
    local ko_KR = (function (t)
        local wday = {"일", "월", "화", "수", "목", "금", "토"}
        for i, v in ipairs(_wday) do
            t[v], t[v:sub(1,3)] = wday[i] .. "요일", wday[i]
        end
        for i, v in ipairs(_month) do
            t[v], t[v:sub(1,3)] = i .. "월", i .. "월"
        end
        t.AM, t.PM = "오전", "오후"
        return t
    end)({})
    local ru_RU = (function (t)
        local wday_l = {"воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"}
        local wday_s = {"вс", "пн", "вт", "ср", "чт", "пт", "сб"}
        local month_l = {"январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"}
        local month_s = {"янв.", "февр.", "март", "апр.", "май", "июнь", "июль", "авг.", "сент.", "окт.", "нояб.", "дек."}
        for i, v in ipairs(_wday) do
            t[v], t[v:sub(1,3)] = wday_l[i], wday_s[i]
        end
        for i, v in ipairs(_month) do
            t[v], t[v:sub(1,3)] = month_l[i], month_s[i]
        end
        t.AM, t.PM = "AM", "PM"
        return t
    end)({})
    local zh_CN, zh_TW = (function (t1, t2)
        local wday = {"日", "一", "二", "三", "四", "五", "六"}
        local month = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"}
        for i, v in ipairs(_wday) do
            t1[v] ,t1[v:sub(1,3)], t2[v] ,t2[v:sub(1,3)] = "星期" .. wday[i], "周" .. wday[i], "星期" .. wday[i], "週" .. wday[i]
        end
        for i, v in ipairs(_month) do
            t1[v], t1[v:sub(1,3)], t2[v], t2[v:sub(1,3)] = month[i] .. "月", i .. "月", i .. "月", i .. "月"
        end
        t1.AM, t1.PM, t2.AM, t2.PM = "上午", "下午", "上午", "下午"
        return t1, t2
    end)({}, {})
    local return_key = {
        __index = function (_, key)
            return key
        end
    }
    return (function (tbl)
        local t1, t2 = {},{}
        for _, v in ipairs(tbl) do
            local locale, name = table.unpack(v)
            t1[name] = setmetatable(locale, return_key)
            t2[#t2+1] = name
        end
        return t1, t2
    end)({
        {{}, "None"},-- English
        {zh_CN, "Chinese (Simplified)"},
        {zh_TW, "Chinese (Traditional)"},
        {ja_JP, "Japanese"},
        {ko_KR, "Korean"},
        {ru_RU, "Russian"},
        {es_ES, "Spanish"},
    })
end)()

local os_date = (function ()
    -- windowsでOBSが落ちなかったもの + %P, %k, %l
    -- %k, %l は変更を加えてあるので strftime と同じではない
    local expand = {
        ["%R"] = "%%H:%%M",
        ["%T"] = "%%H:%%M:%%S",
        ["%X"] = "%%H:%%M:%%S",
        ["%c"] = "%%a %%b %%e %%H:%%M:%%S %%Y",
        ["%r"] = "%%I:%%M:%%S %%p",
        ["%x"] = "%%m/%%d/%%y",
    }
    local allow_format = {
        "%%", "%A", "%B", "%H", "%I", "%M", "%P", "%S",
        "%Y", "%a", "%b", "%d", "%e", "%j", "%k", "%l",
        "%m", "%n", "%p", "%u", "%w", "%y", "%z"
    }
    local __ = function (f, str)
        if f == "%a" or f == "%A" then
            return locale[wday][str] end
        if f == "%b" or f == "%B" then
            return locale[month][str] end
        if f == "%p" then
            return locale[ampm][str] end
        if f == "%P" then
            return locale[ampm][str]:lower() end
        if f == "%k" and str == "00" then
            return "24" end
        if f == "%l" and str == "12" then
            return "00" end
        return str
    end
    return function (format)
        for _, v in ipairs({"%R", "%T", "%X", "%c", "%r", "%x"}) do
            format = string.gsub(format, "%" .. v, expand[v])
        end
        for _, v in ipairs(allow_format) do
            if v == "%%" then v = "%" .. v end
            local str, x = "%" .. v, nil
            if v == "%P" then v, x = "%p", "%P" end
            if v == "%k" then v, x = "%H", "%k" end
            if v == "%l" then v, x = "%I", "%l" end
            if string.match(format, str) then
                local success, result = pcall(string.gsub, format, str, __(x or v, os.date(v)))
                if not success then
                    return os.date(default_format)
                end
                format = result
            end
        end
        return format
    end
end)()

local timer_cb = function ()
    local source = obs.obs_get_source_by_name(source_name)
    local settings = obs.obs_data_create()
    obs.obs_data_set_string(settings, "text", os_date(datetime_format))
    obs.obs_source_update(source, settings)
    obs.obs_data_release(settings)
    obs.obs_source_release(source)
end

local activate = (function (cb)
    local timer_adjust = function ()
        local date = os.date("*t")
        obs.timer_add(function ()
            if date.sec ~= os.date("*t").sec then
                obs.remove_current_callback()
                obs.timer_add(cb, 1000)
            end
        end, 10)
    end
    local timer = {
        [true]  = function () timer_adjust() end,
        [false] = function () obs.timer_remove(cb) end,
    }
    return function (activating)
        if activated == activating then return end
        activated = activating
        timer[activating]()
    end
end)(timer_cb)

----------------------------------------------------------

function script_description()
    return "Sets a text source to act as a clock when the source is active."
end

function script_defaults(settings)
    obs.obs_data_set_default_string(settings, "format", default_format)
    obs.obs_data_set_default_bool(settings, "option", false)
    obs.obs_data_set_default_bool(settings, "note", false)
    obs.obs_data_set_default_string(settings, "note_text", default_note)
end

function script_update(settings)
    activate(false)

    source_name     = obs.obs_data_get_string(settings, "source")
    datetime_format = obs.obs_data_get_string(settings, "format")
    option_visible  = obs.obs_data_get_bool(settings, "option")
    month           = obs.obs_data_get_string(settings, "month")
    wday            = obs.obs_data_get_string(settings, "wday")
    ampm            = obs.obs_data_get_string(settings, "ampm")
    note_visible    = obs.obs_data_get_bool(settings, "note")

    local source = obs.obs_get_source_by_name(source_name)
    if source ~= nil then
        local active = obs.obs_source_showing(source)
        obs.obs_source_release(source)
        activate(active)
    end
end

function script_load(settings)
    local activate_signal = function (bool)
        return function (cd)
            local source = obs.calldata_source(cd, "source")
            if (source_name == obs.obs_source_get_name(source)) then
                activate(bool)
            end
        end
    end

    local sh = obs.obs_get_signal_handler()
    obs.signal_handler_connect(sh, "source_show", activate_signal(true))
    obs.signal_handler_connect(sh, "source_hide", activate_signal(false))
end

function script_properties()
    local root = obs.obs_properties_create()

    obs.obs_properties_add_text(root, "format", "Datetime format", obs.OBS_TEXT_DEFAULT)
    local refresh_source_list = (function(prop)
        return function ()
            local sources = obs.obs_enum_sources()
            obs.obs_property_list_clear(prop)
            obs.obs_property_list_add_string(prop, "", "")
            if sources ~= nil then
                for _, source in ipairs(sources) do
                    local id = obs.obs_source_get_id(source)
                    if id == "text_gdiplus" or id == "text_gdiplus_v2" or id == "text_ft2_source" then
                        local name = obs.obs_source_get_name(source)
                        obs.obs_property_list_add_string(prop, name, name)
                    end
                end
            end
            obs.source_list_release(sources)
            return true
        end
    end)(obs.obs_properties_add_list(root, "source", "Text Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING))
    obs.obs_properties_add_button(root, "refresh_button", "Refresh list of sources", refresh_source_list)
    refresh_source_list()
    local option = obs.obs_properties_create()
    local translate = obs.obs_properties_create()
    obs.obs_property_set_modified_callback(
        obs.obs_properties_add_group(root, "option", "Option", obs.OBS_GROUP_CHECKABLE, option),
        function (props, property, settings)
            obs.obs_property_set_visible(obs.obs_properties_get(props, "translate"), option_visible)
            return true
        end)
    obs.obs_property_set_visible(obs.obs_properties_add_group(option, "translate", "Translate", obs.OBS_GROUP_NORMAL, translate), option_visible)
    local month = obs.obs_properties_add_list(translate, "month", "Month (%b, %B)", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
    local wday = obs.obs_properties_add_list(translate, "wday", "Weekday (%a, %A)", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
    local ampm = obs.obs_properties_add_list(translate, "ampm", "AM/PM (%p, %P)", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
    for _, v in ipairs(locale_keys) do
        for _, prop in pairs({month, wday, ampm}) do
            obs.obs_property_list_add_string(prop, v, v)
        end
    end
    local note = obs.obs_properties_create()
    obs.obs_property_set_modified_callback(
        obs.obs_properties_add_group(root, "note", "Note", obs.OBS_GROUP_CHECKABLE, note),
        function (props, property, settings)
            obs.obs_property_set_visible(obs.obs_properties_get(props, "note_text"), note_visible)
            return true
        end)
    obs.obs_property_set_visible(obs.obs_properties_add_text(note, "note_text", nil, obs.OBS_TEXT_MULTILINE), note_visible)

    return root
end
